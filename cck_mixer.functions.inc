<?php

/*
 *  Get order of mixed fields
 */
function cck_mixer_get_fields($node)
{
	static $output = NULL;
	
	if($output == NULL)
	{
		$output = array();
		
		$order = cck_mixer_get_order($node->nid);
		foreach ($order as $eachOrder) 
		{
			$field = $eachOrder->field;
			
			$fieldName = $field->formItemCCK;
			$fieldIndex = intval($eachOrder->cckIndex);			
			
			$objectCCK = $node->$fieldName;
			$fieldCCK = $objectCCK[$fieldIndex];
			if(isset($fieldCCK))
			{				
				$output[] = array( 	'key' => $fieldName, 
									'value' => $fieldCCK);				
			}
		}		
	}	
	
	return $output;
}

/*
 * Get index of field
 */
function cck_mixer_index_pos_field($node, $fieldName, $offset = 0)
{
	$order = cck_mixer_get_fields($node);
		
	for($i = $offset; $i < count($order); $i++)
	{ 
		if($order[$i]['key'] == $key)
		{
			return $i;
		}		
	}
	
	return -1;
}
