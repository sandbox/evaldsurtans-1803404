(function($){

	var asketic = asketic || {};
	
	asketic.CCKMixer = function()
	{ 
	   //If all previous order of items has been restored
	   self.initialized = false; 
		
	   self.fieldTypes = [];
	   	
	   self.$cckMixerOrderJson = $('#edit-cck-mixer-order');
	   self.cckMixerOrder = $.evalJSON(self.$cckMixerOrderJson.val());
	   self.orderIndex = 0;
		
	   self.$sontentFrame = $(".column-wrapper:first");
	   
	   self.$titleField = self.$sontentFrame.find('#edit-title-wrapper');
	   
	   self.$titleField.after('<fieldset class="fieldset titled fieldset-cck-mix"><legend><span class="fieldset-title"><span class="icon"></span><span class="active">Fields</span></span></legend><div id="cck-mixer-content-list" class="fieldset-content clear-block"></div></fieldset>');
	   self.$cckField = self.$titleField.next().find('div:first');
	   
	   //Insertion of elements (if both are null then insert at the end)
	   self.$insertBefore = null;
	   self.$insertAfter = null;
	   
	   var $body = $('body');
	   $body.append('<ul id="cck-mixer-menu" class="contextMenu"></ul>');
	   self.$cckMenu = $('#cck-mixer-menu');	    
	   
	   var formItemIndex = 0;
	   var formItemCCK = '';
	   var formItemTableId = '';	   
	   
	   //Hide default CCK blocks
	   self.$sontentFrame.find('> div:visible').each(function(){
	   		var div = $(this);
	   		var id = div.attr('id');
	   		if(id.indexOf('field-') >= 0)
	   		{
	   			//Use only fields that can have multiple values
	   			//Field with only one value will be left untouched
	   			var th = div.find('th:first');
	   			if(th.length)
	   			{
	   				var title = th.html();
	   				title = $.trim(title.replace(':', ''));
	   				div.hide();
	   				
	   				formItemIndex = 0;
	   				formItemId = '';
	   				
	   				div.find("td.content-multiple-drag").each(function(){
	   					var td = $(this).next(); //td that hold form item
	   					
	   					var eachItem = td.find('> *:first');
	   					var eachItemName = eachItem.find('[name]:first').attr('name');
	   					
	   					//Identify value field
	   					if(eachItemName.indexOf('][') > 0)
	   					{
	   						//Get index
	   						var temp = eachItemName.split('][');
	   						temp = temp[0].split('[');
	   						
	   						formItemCCK = temp[0];			
	   						formItemTableId = td.parents('table:first').attr('id');
	   						
	   						var index = parseInt(temp[1]);
	   						
	   						if(formItemIndex < index)
	   						{
	   							//Least index
	   							formItemIndex = index;
	   						}
	   					}
	   				});
	   				
	   				self.fieldTypes.push({
	   					id : id,
	   					title : title,
	   					formItemIndex : formItemIndex,
	   					formItemTableId : formItemTableId,
	   					formItemCCK : formItemCCK
	   				});
	   				
	   				self.$cckMenu.append('<li><a href="#" data-fieldtype-index="' + (self.fieldTypes.length - 1).toString() + '">' + title + '</a></li>');
	   			}
	   		}
	   });
	   
	   
	   self.$cckField.append('<input typ="button" class="form-submit add-field" value="Add Field" />');
	   self.$buttonAdd = self.$cckField.find('input.add-field:last');   
	    
	   //Setup sortable interface
	   self.$sortable = $(self.$cckField[0]).sortable({
			connectWith: '.column-wrapper ul',
			handle: 'legend',
			cursor: 'move',
			placeholder: 'placeholder-fieldset-draggable',
			forcePlaceholderSize: true,
			opacity: 0.4,
			stop: function(event, ui){
									
			}
		})
	    
	    self.waitingOnCCK = false;
	    self.waitingOnInner = false;
	    self.selectedFieldtype = null;
	    
	    self.$thobber = null;
	    
	    //Context menu for insertion
		$body.append('<ul id="cck-mixer-menu-context" class="contextMenu"></ul>');
	   	self.$ccContextkMenu = $('#cck-mixer-menu-context');
	   	self.$ccContextkMenu.append('<li><a data-where="before" href="#">Insert before</a></li>')
	   	self.$ccContextkMenu.append('<li><a data-where="after" href="#">Insert after</a></li>')
	   	
	   	self.$ccContextkMenu.find('a').mousedown(function(){
	   		var $a = $(this);
	   		var whereToInsert = $a.attr('data-where');	   		
	   		
	   		var $elementBehindClick = null;
	   		var deltaPositionElementBehind = Number.MAX_VALUE;
	   		
	   		//Detect element behind
	   		var position = self.$ccContextkMenu.offset();
	   		self.$cckField.find('> fieldset').each(function(){
	   			var $fieldset = $(this);
	   			
	   			var delta = Math.abs($fieldset.offset().top + $fieldset.height() / 2 - position.top);
	   			if(deltaPositionElementBehind > delta)
	   			{
	   				deltaPositionElementBehind = delta;
	   				$elementBehindClick = $fieldset;
	   			}
	   		});
	   		
	   		if($elementBehindClick)
	   		{
		   		if(whereToInsert == 'after')
		   		{
		   			//After
		   			self.$insertAfter = $elementBehindClick;
		   		}
		   		else
		   		{
		   			//Before
		   			self.$insertBefore = $elementBehindClick;
		   		}
	   		}	   		
	   	});
	   	
	   	var $temp = self.$cckMenu.clone().removeAttr('id');	   	
	   	self.$ccContextkMenu.find('li:last').append($temp);	    
	   
	    //Handle pressing on button (before context menu)
	    self.$buttonAdd.mousedown(function(){
	    	//Reset selection for inserting elements (defaults to the end) 	    	
	    	self.$insertBefore = null;
	   		self.$insertAfter = null;   
	   		//Next contextMenu will be called
	    });
	   
	    //Menu of types	    
	   	self.$buttonAdd.add(self.$ccContextkMenu).contextMenu({
			menu: self.$cckMenu.attr('id'),
			anyButton: true
		},
			function(sel, el, pos) {
				var fieldTypeIndex = parseInt($(sel).attr('data-fieldtype-index'));
				var fieldType = self.fieldTypes[fieldTypeIndex];
				
				self.onFieldItemChoosen(fieldType);
		},
			function(){
				self.$buttonAdd.trigger('blur'); //Remove selection carret from botton
				return self.isEnabled();
		});
		
		self.isEnabled = function() {
			return (!self.waitingOnCCK && !self.waitingOnInner && self.initialized);
		}
	    
	    self.addThobber = function() {
	    	if(!self.$thobber)
	    	{
		    	var $button = self.$buttonAdd;
		    	self.$thobber = $('<div class="ahah-progress ahah-progress-throbber" style="display: block;"><div class="throbber">&nbsp;</div></div>');
			    $button.after(self.$thobber);
		    }
	    }
	    
	    //Insertion of fields
	    //Proccess adding fields
	    self.onFieldItemChoosen = function(fieldType) {
	    	
	    	var $button = self.$buttonAdd; 
	    	$button.trigger('blur'); //Remove selection carret from botton
	    	    	
	    	//Not the same as self.isEnabled (this will be called before self.initialized to add initial items)
	    	if(self.waitingOnCCK || self.waitingOnInner)
	    	{
	    		return false;
	    	}
	    	
	    	self.selectedFieldtype = fieldType;
	    	
	    	if(self.$thobber)
	    	{
	    		self.$thobber.remove();
	    		self.$thobber = null;
	    	}	    	
	    	    	
	    	//Get field
	    	var $formItemTable = $('#'+ fieldType.formItemTableId);
	    	var $formItemInput = $formItemTable.find('[name^="' + fieldType.formItemCCK + '[' + fieldType.formItemIndex + ']"]:not([name$="weight]"],[name$="remove]"]):first');
	    		    	
	    	//Check if have next item available
	    	if(!$formItemInput.length)
	    	{
	    		self.$cckField.addClass('wait'); 
	    		
	    		//No ajax requests needed when restoring fields (they should be all on the form)
	    		if(self.initialized)
	    		{
		    		//Request new one
		    		self.waitingOnCCK = true;
		    		var $addMoreButton = $('#'+ fieldType.id).find('.content-add-more input:first');
		    				    		
		    		jQuery($addMoreButton[0]).trigger('mousedown');
		    		self.addThobber();		    		
	    		}
	    		else
	    		{	    			
	    			console.log('Error: CCK Mixer could not find field to restore order!', fieldType);
	    		}
	    		
	    		return false;
	    	}	    	
	    	
	    	var $fieldTd = $formItemInput.parents('td:first');
	    	var $fieldToAdd = $fieldTd.find('> *');	    	
	    	
	    	//Glocal order index for identification
	    	self.orderIndex++;
	    	
	    	//Format field
	    	var $add = $('<fieldset data-type-name="' + fieldType.formItemCCK + '" data-order-index="' + self.orderIndex.toString() + '" class="fieldset titled fieldset-draggable"><legend><span class="fieldset-title"><span class="icon"></span><span class="active">' + fieldType.title + '</span></span></legend><div class="fieldset-content clear-block"></div></fieldset>');	   
	    	
	    	//Add to sortable
	    	if(self.$insertBefore)
	    	{
	    		self.$insertBefore.before($add);
	    		$add = self.$insertBefore.prev();	    		
	    	}
	    	else if(self.$insertAfter)
	    	{
	    		self.$insertAfter.after($add);
	    		$add = self.$insertAfter.next();
	    		
	    	}
	    	else
	    	{
	    		self.$buttonAdd.before($add);	
	    		$add = self.$buttonAdd.prev();
	    	}
	    		    
	    	
	    	//Reset selection for inserting elements (defaults to the end) 	    	
	    	self.$insertBefore = null;
	   		self.$insertAfter = null;   
	    	 	    	
	    	$add.find('div:first').append($fieldToAdd);
	    	
	    	//Context menu to insert item after/before other item
	    	$add.contextMenu({
				menu: self.$ccContextkMenu.attr('id'),
				disabledButtons: true
			},
			function(sel, el, pos) {				
					//Next contextMenu will be called automatically
			},
			function(){									
					return self.isEnabled();					
			});
	    	
	    	//Bind inner submits
	    	$fieldToAdd.find('input[type="submit"]:not(.asketic-processed)').addClass('asketic-processed').bind('mousedown keydown', function(event){
	    		self.self.waitingOnInner = true;
	    		self.addThobber();
	    	});
	    	
	    	//Check if content is hidden by class
	    	if($fieldToAdd.length == 1 && ($fieldToAdd.hasClass('hidden') || $fieldToAdd.find('> .hidden').length))
	    	{
	    		$add.addClass('hidden');
	    	}
	    	
	    	//Register order	    	
	    	self.cckMixerOrder.push({
	    		orderIndex : self.orderIndex,
	    		cckIndex : fieldType.formItemIndex,
	    		field : fieldType
	    	});
	    	
	    	//Remove duplicates
	    	if(self.initialized)
	    	{	    		
	    		$formItemTable.find('td').find('input, *[name]:not([name$="weight]"],[name$="remove]"])').remove();
	    	}	    	
	    	
	    	//Remove button	    	
			$add.find('.fieldset-title:first').append('<a title="Remove" class="content-multiple-remove-button" href="#"></a>');
			var $removeButton = $add.find('.content-multiple-remove-button');
			$removeButton.click(function(event){
				event.preventDefault();
				var $removeButton = $(this);
				
				var $fieldset = $removeButton.parents('.fieldset-draggable');	
				$fieldset.toggleClass('content-multiple-removed-row');
				
				if($fieldset.hasClass('content-multiple-removed-row'))
				{
					$removeButton.attr('title', 'Undo');
				}	
				else
				{
					$removeButton.attr('title', 'Remove');
				}							
			});
	    	
	    	//Refresh sortable UI
	    	self.$sortable.sortable('refresh');
	    	
	    	return true;
	    }
	    
	    self.save = function(){
	    		    	 
	    	 var cckFinalOrder = new Array();
	    	 
	    	 self.$cckField.find('> .fieldset-draggable').each(function(){
	    	 	var $fieldset = $(this);
	    	 	
	    	 	//Unbind delete buttons
	    	 	$fieldset.find('.content-multiple-remove-button').unbind('click');
	    	 	
	    	 	//Process removed nodes
	    	 	if($fieldset.is('.content-multiple-removed-row'))
	    	 	{
	    	 		//It will leave it as empty field in the data
	    	 		$fieldset.find('.fieldset-content').remove();
	    	 	}
	    	 	else
	    	 	{
	    	 		var orderIndex = parseInt( $fieldset.attr('data-order-index') );
	    	 		
	    	 		for(var i = 0; i < self.cckMixerOrder.length; i++ )
		    	 	{
		    	 		var fieldData = self.cckMixerOrder[i];
		    	 		if(fieldData.orderIndex == orderIndex)
		    	 		{		    	 					    	 			
		    	 			cckFinalOrder.push(fieldData);
		    	 		}
		    	 	}	
	    	 	}	    	 	
	    	 });	    	 
	    	 
	    	 //Rearrange order
	    	 for(var i = 0; i < cckFinalOrder.length; i++ )
	    	 {
	    	 	cckFinalOrder.orderIndex = i;
	    	 }
	    	 
	   		 self.$cckMixerOrderJson.val( $.toJSON(cckFinalOrder) );	   		   		
	    }
	    
	    //Handle saving CCK Mixer order
	    $('form#node-form').submit(self.save);	    
	    	    
	    //Initialize previous order of items
	    var tempOrder = self.cckMixerOrder; 
	    self.cckMixerOrder = new Array(); //Flush existing order	    
	    for(var i = 0; i < tempOrder.length; i++)
	    {
	    	var cckOrderField = tempOrder[i];
	    	cckOrderField.field.formItemIndex = cckOrderField.cckIndex;
	    	self.onFieldItemChoosen(cckOrderField.field);
	    }	    
	    self.initialized = true;
	    
	
	    /* Destruct instance */
	    self.release = function()
	    {    				
			self.$defaultContent.show();
			self.$gridsterDiv.remove();
	    }
	
	    return self;
	};
	
	Drupal.behaviors.CCKMixer = function() {
		
		if(self.waitingOnInner)
		{
			self.waitingOnInner = false;
			
			if(self.$thobber)
	    	{
	    		self.$thobber.remove();
	    		self.$thobber = null;
	    	}
		}
		
		setTimeout(function(){
			//Handle request after adding CCK field
			var self = window.cckMixer;
			if(self.waitingOnCCK)
			{			
				self.$cckField.removeClass('wait');
				
				self.waitingOnCCK = false;
				self.selectedFieldtype.formItemIndex++;			
				
				//Add it 
				self.onFieldItemChoosen(self.selectedFieldtype);
			}
		}, 1000);
	}
	
	$(document).ready(function(){
		window.cckMixer = new asketic.CCKMixer();
	});
	
})(jQuery_1_3_2);
